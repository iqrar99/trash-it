from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request, HttpRequest
from importlib import import_module
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.conf import settings
from .views import *
from .models import *
from django.contrib.auth.models import User

# class buangsampah_test(TestCase):
#     def test_url_exist(self):
#         response = Client().get('')
#         self.assertEqual(response.status_code, 200)

#     def test_template_exist(self):
#         response = Client().get('/buangsampah/')
#         self.assertTemplateUsed(response, 'form.html')

#     def test_using_index_func(self):
#         found = resolve('/buangsampah/')
#         self.assertEqual(found.func, snippet_detail)

#     def test_url_not_exist(self):
#         response = Client().get('/oi')
#         self.assertEqual(response.status_code, 404)

    # def test_ada_tulisan_berat_sampah(self):
    #     request = HttpRequest()
    #     response = snippet_detail(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Berat Sampah', html_response)
    #     self.assertIn('Lokasi Rumah', html_response)
    #     self.assertIn('Kategori Sampah', html_response)

#     @classmethod
#     def setUpTestData(cls):
#         Snippet.objects.create(
#             berat_Sampah = "30kg",
#             lokasi_Rumah = "Perumahan Ciparigi Indah"
#         )
        
#     def test_first_name_label(self):
#         # Set up non-modified objects used by all test methods
#         snippet = Snippet.objects.get(id=1)
#         field_label = snippet._meta.get_field('berat_Sampah').verbose_name
#         self.assertEquals(field_label, 'berat Sampah')

class TestSubmitBuangSampah(TestCase):


    def test_buang_sampah_not_loggedin(self):
        response = self.client.get('/buangsampah/')
        self.assertEqual(response.status_code, 302)
    
    def test_buang_sampah_loggedin(self):
        self.client.post('/login/', {'username' : 'user1234', 'password' : 'simplepassword'})
        response = self.client.get('/buangsampah/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form.html')
    
    def test_submit_form_success(self):
        self.client.post('/login/', {'username' : 'user1234', 'password' : 'simplepassword'})
        response = self.client.post('/buangsampah/', {
            'berat_sampah' : ['2'],
            'lokasi_id' : ['3603051006'],
            'lokasi_text' : ['Jalan Cempaka Putih'],
            'kategori_sampah' : ['Plastik', 'Kertas', 'Sisa Makanan'],
        })
        self.assertIn('Sampah berhasil di submit', response.content.decode())
    
    def test_submit_form_error(self):
        self.client.post('/login/', {'username' : 'user1234', 'password' : 'simplepassword'})

        response = self.client.post('/buangsampah/', {
            'berat_sampah' : ['2'],
            'lokasi_id' : ['3603051006'],
            'lokasi_text' : ['Jalan Cempaka Putih'],
        })
        self.assertIn('Input form tidak valid', response.content.decode())
        response = self.client.post('/buangsampah/', {
            'berat_sampah' : ['-2'],
            'lokasi_id' : ['3603051006'],
            'lokasi_text' : ['Jalan Cempaka Putih'],
            'kategori_sampah' : ['Plastik', 'Kertas', 'Sisa Makanan'],
        })
        self.assertIn('Input form tidak valid', response.content.decode())
        response = self.client.post('/buangsampah/', {
            'berat_sampah' : ['2'],
            'lokasi_text' : ['Jalan Cempaka Putih'],
            'kategori_sampah' : ['Plastik', 'Kertas', 'Sisa Makanan'],
        })
        self.assertIn('Input form tidak valid', response.content.decode())

#    def test_ada_tulisan_berat_sampah(self):
#        request = HttpRequest()
#        response = self.client.get('/buangsampah/')
#        html_response = response.content.decode('utf8')
#        self.assertIn('Berat Sampah', html_response)
#        self.assertIn('Alamat', html_response)
#        self.assertIn('Kategori Sampah', html_response)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User.objects.create_user('user1234', 'user1234@host.com', 'simplepassword')
        user.first_name = 'User'
        user.last_name = 'Name'
        user.save()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
    pass

from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import SubmitSampah


# def contact(request):

#     if request.method == 'POST':
#         form = ContactForm(request.POST)
#         if form.is_valid():

#             name = form.cleaned_data['name']
#             email = form.cleaned_data['email']
#             category = form.cleaned_data['category']

#             print(name,email,category)

#     form = ContactForm()
#     return render(request, 'form.html', {'form': form})


# def snippet_detail(request):
#     if request.method == 'POST':
#         form = SnippetForm(request.POST)
#         if form.is_valid():
#             print("VALID")
#             form.save()
    
#     form = SnippetForm()
#     return render(request, 'form.html', {'form': form})

@login_required(login_url='/login/')
def submit_sampah(request):
    context = {
        'form' : SubmitSampah()
    }

    if request.method == 'POST':
        data = request.POST.copy()
        data['owner_id'] = request.user.id
        post_form = SubmitSampah(data)
        if post_form.is_valid() and int(data['berat_sampah']) > 0:
            post_form.save()
            context['success'] = 'Sampah berhasil di submit'
        else:
            context['form'] = post_form
            context['error'] = 'Input form tidak valid'

    return render(request, 'form.html', context)

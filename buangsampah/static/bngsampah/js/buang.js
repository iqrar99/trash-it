/*eslint-env jquery*/

$(function () {
    let select_provinsi = $('select#id_nama_provinsi');
    let select_kota = $('select#id_nama_kota');
    let select_kec = $('select#id_nama_kecamatan');
    let select_desa = $('select#id_nama_desa');

    $.getJSON(`https://dev.farizdotid.com/api/daerahindonesia/provinsi`, function (data) {
        let list_provinsi = data.semuaprovinsi;
        list_provinsi.forEach(element => {
            select_provinsi.append(
                `<option value="${element.id}">${element.nama}</option>`
            );
        });
    });

    select_provinsi.change(function () {
        let value = select_provinsi.val();
        select_kota.val('none');
        select_kec.val('none');
        select_desa.val('none');
        if (value != 'none') {
            select_kota.removeAttr('disabled');
            $.getJSON(`https://dev.farizdotid.com/api/daerahindonesia/provinsi/${value}/kabupaten`, function (data) {
                $('option.kota-value').remove();
                let list_kabupaten = data.kabupatens;
                list_kabupaten.forEach(element => {
                    select_kota.append(
                        `<option value="${element.id}" class="kota-value">${element.nama}</option>`
                    );
                });
            });
        } else {
            select_kota.attr('disabled', 'disabled');
            $('option.kota-value').remove();
            select_kec.attr('disabled', 'disabled');
            $('option.kec-value').remove();
            select_desa.attr('disabled', 'disabled');
            $('option.desa-value').remove();
        }
    });

    select_kota.change(function () {
        let value = select_kota.val();
        select_kec.val('none');
        select_desa.val('none');
        if (value != 'none') {
            select_kec.removeAttr('disabled');
            $.getJSON(`https://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/${value}/kecamatan`, function (data) {
                $('option.kec-value').remove();
                let list_kecamatan = data.kecamatans;
                list_kecamatan.forEach(element => {
                    select_kec.append(
                        `<option value="${element.id}" class="kec-value">${element.nama}</option>`
                    );
                });
            });
        } else {
            select_kec.attr('disabled', 'disabled');
            $('option.kec-value').remove();
            select_desa.attr('disabled', 'disabled');
            $('option.desa-value').remove();
        }
    });

    select_kec.change(function () {
        let value = select_kec.val();
        select_desa.val('none');
        if (value != 'none') {
            select_desa.removeAttr('disabled');
            $.getJSON(`https://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/kecamatan/${value}/desa`, function (data) {
                $('option.desa-value').remove();
                let list_desa = data.desas;
                list_desa.forEach(element => {
                    select_desa.append(
                        `<option value="${element.id}" class="desa-value">${element.nama}</option>`
                    );
                });
            });
        } else {
            select_desa.attr('disabled', 'disabled');
            $('option.desa-value').remove();
        }
    });
});
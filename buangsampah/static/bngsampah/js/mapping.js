/* eslint-env jquery, leaflet, rc-leaflet */

let L = window.L;

$(function () {
    let select_provinsi = $('select#id_nama_provinsi');
    let select_kota = $('select#id_nama_kota');
    let select_kec = $('select#id_nama_kecamatan');
    let select_desa = $('select#id_nama_desa');

    let mymap = L.map('mapid').setView([-3.38029,114.64810], 6);

    let mapUpdater;
    let marker;

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        accessToken: 'pk.eyJ1IjoicWFkemlmaSIsImEiOiJjazNzZTR2Z3IwNXo3M2pxbTg4dWg1aXd4In0.2-GBdBgdoUNtoGO1AKFo8Q'
    }).addTo(mymap);

    mymap.on('click', function (e) {
        let loc = e.latlng;
        $.getJSON(`https://geocode.xyz/${loc.lat},${loc.lng}?geoit=JSON&geojson=1`, function (data) {
            console.log(data);
        });
    });

    // select_provinsi.change(function () {
    //     let value = select_provinsi.val();
    //     if (value != 'none') {
    //         if (mapUpdater != undefined) clearTimeout(mapUpdater);
    //         mapUpdater = setTimeout(updateMapLocation, 3000);
    //     }
    // });

    $('.select-lokasi').change(function () {
        let value = $(this).val();
        if (value != 'none') {
            if (mapUpdater != undefined) clearTimeout(mapUpdater);
            mapUpdater = setTimeout(updateMapLocation, 3000);
        }
    })

    function updateMapLocation() {
        let nama = {
            'provinsi' : select_provinsi.find(':selected').text(),
            'kota' : select_kota.find(':selected').text(),
            'kecamatan' : select_kec.find(':selected').text(),
            'desa' : select_desa.find(':selected').text(),
        };

        let location_query = '';
        let zoom_level = 9;

        if (nama.provinsi != '--') {
            location_query = `${nama.provinsi}${location_query}`;
        } else {
            if (marker != undefined) mymap.removeLayer(marker);
        }
        if (nama.kota != '--') {
            location_query = `${nama.kota}, ${location_query}`;
            zoom_level = 11;
        }
        if (nama.kecamatan != '--') {
            location_query = `${nama.kecamatan}, ${location_query}`;
            zoom_level = 13;
        }
        if (nama.desa != '--') {
            location_query = `${nama.desa}, ${location_query}`;
            zoom_level = 15;
        }

        // console.log(location_query);
        $.getJSON(`https://geocode.xyz/${location_query}?geoit=JSON&geojson=1`, function (data) {
            // console.log(data);
            if (marker != undefined) mymap.removeLayer(marker);
            marker = L.marker([data.latt, data.longt]).addTo(mymap);

            mymap.setView([data.latt, data.longt], zoom_level);
        });
    }
});

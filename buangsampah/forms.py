
from django import forms
from .models import Sampah



# class ContactForm(forms.Form):
#     name = forms.CharField(label = 'Berat Sampah')
#     email = forms.EmailField(label = 'E-Mail')
#     category = forms.ChoiceField(label = 'Kategori Sampah', choices=[('plastik', 'Plastik'), ('kertas', 'Kertas'),('sisa makanan', 'Sisa Makanan'),('logam', 'Logam'),('kaca','Kaca'),('lain-lain','Lain-lain')])
#     subject = forms.CharField(required = False)
#     body = forms.CharField(widget = forms.Textarea)
   

#     def __init__ (self, *args, **kwargs):
#         super().__init__(*args, **kwargs)

class SubmitSampah(forms.ModelForm):

    class Meta:
        model = Sampah
        fields = '__all__'

# class SnippetForm(forms.ModelForm):

#     class Meta:
#         model = Snippet
#         exclude = ['status_Diterima']
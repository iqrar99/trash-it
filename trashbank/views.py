from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request, 'new_home.html')

def faq(request):
    return render(request, 'new_faq.html')

def tutorial(request):
    return render(request, 'new_tutorial.html')
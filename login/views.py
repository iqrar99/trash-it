from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from trashbank.views import home

def login_user(request):

    # prevent an account that already login accessing this page
    if 'AnonymousUser' != str(request.user):
        return redirect('home')

    message = ''

    if request.method == 'POST':

        username_login = request.POST['username']
        password_login = request.POST['password']

        user = authenticate(request, username=username_login, password=password_login)
        # print(user)

        if user != None:
            try:
                login(request, user)
            except:
                pass

            return redirect('home')

        else:
            message = 'Wrong username or password'

    context = {
        'message' : message,
    }

    return render(request, 'login.html', context)

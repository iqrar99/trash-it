from django.test import TestCase, Client, RequestFactory            # pragma: no cover
from django.urls import resolve, reverse                            # pragma: no cover
from django.test import LiveServerTestCase
from django.shortcuts import render, resolve_url
from django.contrib.auth.models import User, AnonymousUser
from django.http.response import HttpResponseRedirect


from .views import login_user

class LoginUnitTest(TestCase):
    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_use_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_login_using_register_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_user)

class LoginViewsTest(TestCase):
    def setUp(self):

        # RequestFactory is an object to make a request for testing our views
        self.factory = RequestFactory()

        self.user = User.objects.create_user(
            username='betatester',
            email='tester@secret.com',
            password='test12345',
            first_name='beta',
            last_name='tester',
        )

    def test_signed_in_account_access_login_page_will_get_redirect(self):
        request = self.factory.get('/login/')
        request.user = self.user

        response = login_user(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_login(self):
        request = self.factory.post('/login/')
        request.user = AnonymousUser()
        request.POST = request.POST.copy()
        request.POST['username'] = 'betatester'
        request.POST['password'] = 'test12345'

        response = login_user(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_login_failed(self):
        request = self.factory.post('/login/')
        request.user = AnonymousUser()
        request.POST = request.POST.copy()
        request.POST['username'] = 'xxxxxxxx'
        request.POST['password'] = 'loremipsum'

        response = login_user(request)
        self.assertIn(response.status_code, (200, 302))

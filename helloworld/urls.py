from django.urls import path

from . import views

app_name = 'helloworld'

urlpatterns = [
    path('helloworld/', views.hello, name='hello'),
]
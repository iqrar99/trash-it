from django.http import JsonResponse
from django.shortcuts import render
from buangsampah.models import Sampah

# Create your views here.
def index(request):
    context = {}

    return render(request, 'statistik/index.html', context)

def get_stats(request):
    if request.method == 'POST':
        query = Sampah.objects.all()
        if 'lokasi_id' in request.POST:
            lokasi_id = request.POST['lokasi_id']
            query = query.filter(lokasi_id__regex=rf'^{lokasi_id}.{{{10 - len(lokasi_id)}}}$')

    query = [i.__dict__.copy() for i in query]
    for i in query: del i['_state']

    return JsonResponse(list(query), safe=False)
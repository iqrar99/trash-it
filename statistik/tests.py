from django.test import TestCase

# Create your tests here.

class HalamanStatistikTest(TestCase):

    def test_url_get(self):
        response = self.client.get('/statistik/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'statistik/index.html')
    
    def test_stats_get(self):
        response = self.client.post('/statistik/get_stats/', {'lokasi_id' : '31'})
        self.assertEqual(response.status_code, 200)

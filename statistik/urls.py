from django.urls import path

from . import views

app_name = 'statsk'

urlpatterns = [
    path('', views.index, name='index'),
    path('get_stats/', views.get_stats, name='get'),
]
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import tutorialViews
from django.http import HttpRequest
from unittest import skip

class TutorialTests(TestCase):

    def test_tutorial_url_is_exist(self):
        response = Client().get(reverse('tutorial'))
        self.assertEqual(response.status_code, 200)

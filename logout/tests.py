from django.test import TestCase, Client, RequestFactory            # pragma: no cover
from django.urls import resolve, reverse                            # pragma: no cover
from django.test import LiveServerTestCase
from django.shortcuts import render, resolve_url
from django.contrib.auth.models import User, AnonymousUser
from django.http.response import HttpResponseRedirect

from .views import logout_user

class LogoutUnitTest(TestCase):
    def setUp(self):
        # RequestFactory is an object to make a request for testing our views
        self.factory = RequestFactory()

        self.user = User.objects.create_user(
            username='betatester',
            email='tester@secret.com',
            password='test12345',
            first_name='beta',
            last_name='tester',
        )

    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertIn(response.status_code, (200, 302))


    def test_logout_using_register_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout_user)

class LogoutViewstest(TestCase):

    def setUp(self):
        # RequestFactory is an object to make a request for testing our views
        self.factory = RequestFactory()

        self.user = User.objects.create_user(
            username='betatester',
            email='tester@secret.com',
            password='test12345',
            first_name='beta',
            last_name='tester',
        )

    def test_logout(self):
        request = self.factory.post('/login/')
        request.user = self.user
        request.POST = request.POST.copy()
        request.POST['logout'] = ''

        response = logout_user(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_logout_canceled(self):
        request = self.factory.get('/login/')
        request.user = self.user

        response = logout_user(request)
        self.assertEqual(response.status_code, 200)

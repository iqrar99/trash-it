from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session

@login_required(login_url='/login/')
def logout_user(request):
    if request.method == 'POST':

        # if user click the logout button
        if request.POST['logout'] == '':
            try:
                logout(request)
            except:
                pass

        return redirect('/')

    return render(request, 'logout.html')

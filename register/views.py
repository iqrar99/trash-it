from django.shortcuts import render, redirect, reverse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from trashbank.views import home

def register_user(request):
    # prevent an account that already login accessing this page
    if 'AnonymousUser' != str(request.user):
        return redirect(reverse(home))

    message_username = ''
    message_password = ''

    if request.method == 'POST':
        user_name       = request.POST['username']
        user_password   = request.POST['password']
        user_first_name = request.POST['first_name']
        user_last_name  = request.POST['last_name']

        # check wether the username has been used by another user
        if User.objects.filter(username=user_name).exists():
            message_username = 'Username has been used'

        # check wether username is valid
        if not username_is_valid(user_name):
            message_username = 'Invalid username'

        # check password length
        if len(user_password) < 8:
            message_password = 'Passwords must have a minimum length of 8'

        # valid input
        if len(message_username) == 0 and len(message_password) == 0:
            NewUser = User.objects.create_user(
                username    =user_name,
                password    =user_password,
                first_name  =user_first_name,
                last_name   =user_last_name
            )
            NewUser.save()

            return redirect('/login/')

    context = {
        'message_username': message_username,
        'message_password': message_password,
    }

    return render(request, 'register.html', context)


# method for validating username
def username_is_valid(username):
    for c in username:
        if not (c.isalnum() or c in '_.'):
            return False

    return True

from django.test import TestCase, Client, RequestFactory            # pragma: no cover
from django.urls import resolve, reverse                            # pragma: no cover
from django.test import LiveServerTestCase
from django.shortcuts import render, resolve_url
from django.contrib.auth.models import User, AnonymousUser
from django.http.response import HttpResponseRedirect

from .views import register_user

class RegisterUnitTest(TestCase):
    def test_register_url_exists(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_use_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_register_using_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register_user)

class RegisterViewsTest(TestCase):
    def setUp(self):

        # RequestFactory is an object to make a request for testing our views
        self.factory = RequestFactory()

        self.user = User.objects.create_user(
            username='betatester',
            email='tester@secret.com',
            password='test12345',
            first_name='beta',
            last_name='tester',
        )

    def test_register_success(self):
        request = self.factory.post('/register/')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = 'andromeda'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = register_user(request)
        self.assertIn(response.status_code, (200, 302))
        self.assertTrue(type(response) == HttpResponseRedirect)

    def test_register_failed_username_already_used(self):
        request = self.factory.post('/register/')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = 'betatester'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = register_user(request)
        self.assertEqual(response.status_code, 200)

    def test_regiter_failed_username_is_not_valid(self):
        request = self.factory.post('/register/')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = '^%$#(@**$_-)'
        request.POST['password'] = 'milkyway9000'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = register_user(request)
        self.assertEqual(response.status_code, 200)
    
    def test_register_failed_password_is_too_short(self):
        request = self.factory.post('/register/')
        request.user = AnonymousUser()

        request.POST = request.POST.copy()
        request.POST['username'] = '^%$#(@**$_-)'
        request.POST['password'] = 'LOL'
        request.POST['first_name'] = 'space'
        request.POST['last_name'] = 'x'

        response = register_user(request)
        self.assertEqual(response.status_code, 200)
